# CEF Tool Template:

**Name:** [ GraphGen ]

**Purpose/Description:**

[
  GraphGen is a python tool that can be used to build NS2 topology files used
  by the [DETER Testbed](https://www.isi.deterlab.net/index.php3). GraphGen
  supports the use of the [Click Modular Router](https://web.archive.org/web/20160412181839/http://www.read.cs.ucla.edu/click/)
  to enable testbed users to manipulate network traffic flows.
]

**Link to Source:** [ https://github.com/ISIEdgeLab/graphGen ]

**How to Cite:**
[ We do not currently have a peer-review paper, please site our github page above ]

**Language(s):** [ python2, python3 ]

**License:** [ MIT ] 

**Requirements/Constraints:**

  - [ code requires a python2 or python3 interpreter ]
  - [ python interpreter must be defined by environment variable python ]

**How to build:**

  - [ link to containerized build platform (quay, dockerhub, etc) ]
  - [ Documentation ]
  - [ run pip install -r requirements.txt ]

**How to run:**

  - [ ./graphgen/graph_gen.py]

**See also:**

  - [ other links that may help developers with context or usage of tool ]

**CI Badge (optional):**

![GraphGen](https://travis-ci.org/ISIEdgeLab/graphGen.svg?branch=master)
