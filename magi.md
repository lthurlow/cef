
## Montage AGent Infrastructure (MAGI)

MAGI is an experiment workflow management system that helps experimenters orchestrate networking experiments deterministically. It is used to express and automate the procedure of an experiment.

**Link to Source:** 

[https://github.com/deter-project/magi](https://github.com/deter-project/magi) (Last updated: 4/2017)

Modules to MAGI:

[https://github.com/deter-project/magi-modules](https://github.com/deter-project/magi-modules) (Last updated: 6/2016)

with additional agents:

[https://github.com/ISIEdgeLab/magi_modules](https://github.com/ISIEdgeLab/magi_modules) (Last updated: 4/2018)

**How to Cite:**

```
@techreport{Hussain2012,
     title = {{Montage Topology Manager:
Tools for Constructing and Sharing Representative
Internet Topologies}},
     author = {Alefiya Hussain and Jennifer Chen},
     year = {2012},
     institution = {Information Sciences Institute},
     month = {08},
     number = {684},
     url = {ftp://ftp.isi.edu/isi-pubs/tr-684.pdf}
}
```

**Language(s):** python2

**License:** [GNUv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

**Requirements/Constraints:**

[Readme](https://github.com/deter-project/magi/blob/master/README)

**How to build:**

`python setup.py sdist`

**How to run:**

To run/use `MAGI` requires an infrastructure with MAGI installed - such as isi.Deterlab.net

Add `tb-set-node-startcmd $node "sudo python /share/magi/current/magi_bootstrap.py"` to tcl (ns2) experiment definition.

On the experiment's control node, one should be able to orchestrate their experiment through MAGI:
`magi_orchestrator.py --project myProject --experiment myExperiment --events myEvents.aal --logfile orch.log`


**See also:**

[Documentation](https://montage.deterlab.net/magi/contents.html)

[Tutorial](https://montage.deterlab.net/magi/learn.html#magilearn)

**Contact:**

[Alefiya Hussain](https://github.com/alefiyahussain)

[Geoff Lawler](https://github.com/glawler)

[Srivatsan Ravi](https://github.com/kryptos23)

