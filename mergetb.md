# Mergetb

[Merge](https://mergetb.gitlab.io/) is a new testbed implementation, from the USC/ISI group to improve the capabilities of experimenters, operators, and maintainers for experimenting with _any_ device which exposes an interface.

Software for Merge:

[Canopy](https://gitlab.com/mergetb/canopy)

[Nex](https://gitlab.com/mergetb/nex)

[UI](https://gitlab.com/mergetb/tbui)

[Engine](https://gitlab.com/mergetb/engine)

[Site](https://gitlab.com/mergetb/site)

[Sled](https://gitlab.com/mergetb/sled)

[XIR](https://gitlab.com/mergetb/xir)