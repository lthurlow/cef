## Distributed Agents

## Purpose:

Distributed Agents extends the GRPC to provide RPC calls to multiple machines at once.

Define a protobuf file that describes the agent interface, and the distributed agents system will generate a working client-side python agent and a server-side template python agent. All that is needed is for the agent developer to write the server-side python code in the template. Once done, an end user can use the agent to run the code on an arbitrary number of machines. This is done using a python class. Simply call the methods and they run on remote machines.

## Link to Source:
[https://github.com/ISIEdgeLab/distributed_agents](https://github.com/ISIEdgeLab/distributed_agents) (Last updated: 03/2018)

## How to Cite:

Currently no citation available. Please reference github source code.

**Language(s):** python3

**License:** Unknown

**Requirements/Constraints:**

 * `python3`
 * protobuf-compiler
 * sudo apt install -y python-protobuf
 * root / debian operating system

**How to build:**

```./build.sh```

**How to run:**

Unknown

**See also:**

[Source Code](https://github.com/ISIEdgeLab/distributed_agents)
