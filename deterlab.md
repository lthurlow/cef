# Deterlab

Active: Yes

Inception Year: 2003

Funding (Past/Present): DHS, NSF, DARPA


[Deterlab](https://deter-project.org/about_deterlab) describes itself as:
> USC/ISI's DETERLab (cyber DEfense Technology Experimental Research Laboratory) is a state-of-the-art scientific computing facility for cybersecurity researchers engaged in research, development, discovery, experimentation, and testing of innovative cybersecurity technology. DETERLab is a shared testbed providing a platform for research in cybersecurity and serving a broad user community, including academia, industry, and government. To date, DETERLab-based projects have included behavior analysis and defensive technologies including DDoS attacks, worm and botnet attacks, encryption, pattern detection, and intrusion-tolerant storage protocols.

[Main Portal](https://www.isi.deterlab.net/index.php3)

[Documentation](https://docs.deterlab.net/)

[Contact US/Help](https://trac.deterlab.net/wiki/GettingHelp)
