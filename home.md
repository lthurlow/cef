# Deterlab

The Deterlab was funded in 2003 originally by the Department of Homeland Security has since been funded by the National Science Foundation and Defense Advanced Research Projects Agency.

The Deterlab is currently operational and still maintained as of September 2018 with funding until 2020.

[Deterlab](https://deter-project.org/about_deterlab) describes itself as:
> USC/ISI’s DETERLab (cyber DEfense Technology Experimental Research Laboratory) is a state-of-the-art scientific computing facility for cybersecurity researchers engaged in research, development, discovery, experimentation, and testing of innovative cybersecurity technology. DETERLab is a shared testbed providing a platform for research in cybersecurity and serving a broad user community, including academia, industry, and government. To date, DETERLab-based projects have included behavior analysis and defensive technologies including DDoS attacks, worm and botnet attacks, encryption, pattern detection, and intrusion-tolerant storage protocols.

[Main Portal](https://www.isi.deterlab.net/index.php3)

[Documentation](https://docs.deterlab.net/)

[Contact US/Help](https://trac.deterlab.net/wiki/GettingHelp)


# MAGI

http://lminnow0.isi.edu/magi
# GraphGen

http://lminnow0.isi.edu/graph-generator