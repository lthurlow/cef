
## DASH

DASH is an agent platform for building agents that simulate human behavior in a variety of situations in Deter and other environments where group decision-making is mediated by computers. For example, they have been used to model observed behavior in responding to phishing email, downloading and using security software such as Tor or making decisions to control a power plant. In situations such as these, human behavior might differ from the optimal, as may be defined according to decision-theoretic measures or accepted best practice, and these differences impact the behavior of systems under test. This may happen because the typical user of a system has an incorrect or incomplete model of the system's behavior or of its security, or because humans inevitably make mistakes, particularly if their attention is taken with other tasks.

DASH agents model this behavior using a dual-process cognitive architecture. One module within the system models rational behavior, containing sub-modules for reactive planning and for projection using mental models. A second module models instinctive behavior and other reasoning that humans are typically not aware of. The combination of these two modules can account for the effects of cognitive load, time pressure or fatigue on human performance that have been documented in many different domains. The combination can also duplicate some well-known human biases in reasoning, for example the confirmation bias. The DASH platform includes support for teams of agents that communicate with each other and a GUI to control agent parameters and view the state of both modules as the agent executes.

**Link to Source:** 

[dash.zip](https://www.isi.edu/~blythe/Dash/dash-4-26-13.zip) (Last updated: 4/2013)

**How to Cite:**

```
@inproceedings{Blythe12, 
     author={J. Blythe}, 
     booktitle={{2012 5th International Symposium on Resilient Control Systems}},
     title={{A Dual-Process Cognitive Model for Testing Resilient Control Systems}},
     year={2012}, 
     pages={8-12}, 
     doi={10.1109/ISRCS.2012.6309285}, 
     month={Aug},
     url = {http://info.deterlab.net/sites/default/files/files/blythe_isrcs12.pdf},
}
```

**Language(s):** Java

**License:** Unknown.

**Requirements/Constraints:**

You will need to install SWI Prolog and provide its library path to Java.

**How to build:**

[Instructions (beware: docx)](https://www.isi.edu/~blythe/Dash/guide-4-09-13.docx)

**How to run:**

Unknown

**See also:**

[Dash Information](https://www.isi.edu/~blythe/Dash)

**Contact:**

[Jim Blythe](https://www.isi.edu/~blythe/)
